SSH support for the Kobo
========================

The goal of this project is to provide a `KoboRoot.tgz` targeted at
giving out a SSH daemon automatically. While there are other
implementations of this elsewhere, they all use a fairly outdated
build of Dropbear taken out of the Maemo OS (dated `v2014.66` here)
which uses fairly outdated algorithms and wouldn't accept newer
ones. The latest release (`2016.74`) has more decent support for newer
MAC algos and fixes security issues - that probably do not affect the
Kobo in any significant way, but still...

WARNING
=======

This doesn't actually work just yet. For some reason, the daemons
don't get properly started on boot, even if telnet is enabled and
everything. I must be doing something wrong - I need to compare with
the [Preining images](https://www.preining.info/blog/tag/kobo/) and [kobo-aura-remote](https://github.com/dropmeaword/kobo-aura-remote) to see what
exactly.

These days I just use the SSH plugin shipped with [Koreader](https://koreader.rocks/), it
just works and is easy enough. Just drop the SSH `authorized_keys`
file in `.adds/koreader/settings/SSH` and you're done!

Design
======

This project uses the `KoboRoot.tgz` patten which consists of dropping
the file at the root of your Kobo and disconnecting the USB. The
content of the file is then uncompressed over the base filesystem,
which allows us to do all sorts of crazy things.

We use this to deploy the [Dropbear](https://matt.ucc.asn.au/dropbear/dropbear.html) SSH server using the ad-hoc
standard of hijacking the `/etc/inittab` file to run an extra
`/etc/init.d/rcS2` file on boot, which in turn starts an `inetd`
daemon with the `/etc/inetd.local` configuration file, and fire any
client `/mnt/onboard/run.sh` scripts.

Incidentally, we abuse that `run.sh` script to do the initial dropbear
setup which includes:

* blanking the password of the root account (if it's locked)
* disabling the `admin` backdoor
* fix the root home directory to `/root`
* generate the dropbear host keys

We also enable the telnet port in case our dropbear binary doesn't
work. This can be disabled once the user is logged in.

The default username and password on the Kobo is `admin`.

References
==========

 * [how to enable SSH on a Kobo](http://blog.ringerc.id.au/2011/01/enabling-telnet-and-ftp-access-to-kobo.html) - the original tutorial
 * [Preining's documentation on how to enable telnet and SSH](https://www.preining.info/blog/2013/07/ssh-or-telnet-on-the-kobo-glo/) - was
   very useful in building this
 * [Kobo touch hacking](https://wiki.mobileread.com/wiki/Kobo_Touch_Hacking) - the wiki documentation on how to do this
   and more
 * [kobo-aura-remote](https://github.com/dropmeaword/kobo-aura-remote) - similar project, but doesn't automatically
   builds tarballs in CI, only targets the Aura and hardcodes the
   dropbear binaries

Manual deployment
=================

I was able to deploy the Debian-built binary directly to
the Kobo by downloading it from packages.debian.org and extracting it,
thus:

    wget http://ftp.us.debian.org/debian/pool/main/d/dropbear/dropbear-bin_2016.74-2_armhf.deb
    dpkg -x dropbear-bin_2016.74-2_armhf.deb d
    pv d/usr/sbin/dropbear | ssh $host 'cat > /bin/dropbear; chmod +x /bin/dropbear'

`/bin/dropbear` is not the correct location, btw - it's just the one
the Preining images I used configured. A complete `KoboRoot.tgz`
should probably also deploy the `inittab` hack:

    # This is run first except when booting in single-user mode.
    ::sysinit:/etc/init.d/rcS
    ::respawn:/sbin/getty -L ttymxc0 115200 vt100
    ::ctrlaltdel:/sbin/reboot
    ::shutdown:/bin/umount -a -r
    ::restart:/sbin/init
    ::sysinit:/etc/init.d/rcS2

The important part is the last line, which forces the init script to
run an extra script on bootup, which is:

    #!/bin/sh
    mkdir -p /dev/pts
    mount -t devpts devpts /dev/pts
    /usr/sbin/inetd /etc/inetd.conf.local
    # run user supplied commands
    /mnt/onboard/run.sh &

`inetd.conf.local` is:

    22      stream  tcp     nowait  root    /bin/dropbearmulti dropbear -i -B

We could take the rcS2 hack as a "standard" *or* we could deploy our
own (e.g. deploy a `inittab` with `rcS2` *and* a `rc.local` and do our
own things in `rc.local`). In any case, it would be nice to have some
sort of CI that would (or at least, easily and verifiably) a new
`KoboRoot.tgz` image when new releases of dropbear are performed.

We don't need to cross-compile, just make sure we have the arm
architecture in dpkg, fetch the packages with apt-secure correctly
setup, and we're good to use the already existing Debian
infrastructure.

This project was started within the [Wallabako project](https://gitlab.com/anarcat/wallabako/).
