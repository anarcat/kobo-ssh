tarball: Makefile
	if ! ( dpkg --print-architecture ; dpkg --print-foreign-architectures ) | grep arm ; then \
		dpkg --add-architecture armhf ; \
		apt-get update ; \
	fi
	apt-get download dropbear-bin:armhf
	dpkg -x dropbear-bin_*_armhf.deb tmp
	mkdir -p root/usr/sbin
	cp tmp/usr/sbin/dropbear root/usr/sbin
	cd root && tar --owner=root:0 --group=root:0 -c -z -f ../KoboRoot.tgz *
