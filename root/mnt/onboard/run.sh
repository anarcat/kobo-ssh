#!/bin/sh

exec > /mnt/onboard/dropbear-install.log
exec 2>&1

if grep 'root:\*:' /etc/passwd; then
    passwd -d root
    passwd -l admin
fi
if grep 'root:/:/bin/sh' /etc/passwd; then
    sed -i 's#root:/:/bin/sh#root:/root:/bin/sh#' /etc/passwd
fi
if ! [ -d /etc/dropbear ] ; then
    echo "creating ssh host keys"
    mkdir /etc/dropbear
    dropbearkey -t dss -f dropbear_dss_host_key
    dropbearkey -t rsa -f dropbear_rsa_host_key
fi
# erase myself
rm /mnt/onboard/run.sh
